## api 서버 실행
java -jar todos-0.0.1-SNAPSHOT.jar

#### http://localhost:9000/swagger-ui.html


todoList :      GET /api/todos

todoInput :     POST /api/todos/title

todoDelete :    DELETE /api/todos/id

todoDeleteAll : DELETE /api/todos


